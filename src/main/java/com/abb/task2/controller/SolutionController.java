package com.abb.task2.controller;

import com.abb.task2.service.SolutionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/solution")
@RequiredArgsConstructor
public class SolutionController {

    private  final SolutionService service;

    @PostMapping
    public int solution(@RequestBody int[][] A){
        return service.solution(A);
    }
}
