package com.abb.task2.service.impl;

import com.abb.task2.service.SolutionService;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class SolutionServiceImpl implements SolutionService {

    @Override
    public int solution(int[][] A) {
        int count =0;
        int N=A.length;
        int M=A[0].length;
        if (M == 0){
            return 0;
        }
        int [][] clone= Arrays.stream(A).map(int[]::clone).toArray($->A.clone());

        for (int i=0;i<N;i++){
            for (int j=0;j<M;j++){
                if (clone[i][j]>=0){
                    findTheNumberOfDifferentCountries(A,clone,i,j,N,M);
                    count+=1;
                }
            }
        }
        return count;
    }

    private void findTheNumberOfDifferentCountries(int[][]A,int[][]clone,int i,int j,int N,int M ){

        if (clone[i][j]==-1){
            return;
        }
        clone[i][j]=-1;
        if (i+1<N){
            if (A[i+1][j]==A[i][j]){
                findTheNumberOfDifferentCountries(A,clone,i+1,j,N,M);
            }
        }
        if (i-1>=0){
            if (A[i-1][j]==A[i][j]){
                findTheNumberOfDifferentCountries(A,clone,i-1,j,N,M);
            }
        }
        if (j+1<M){
            if (A[i][j+1]==A[i][j]){
               findTheNumberOfDifferentCountries(A,clone,i,j+1,N,M);
            }
        }
        if (j-1>=0){
            if (A[i][j-1]==A[i][j]){
                 findTheNumberOfDifferentCountries(A, clone, i, j-1, N, M);
            }
        }

    }
}
